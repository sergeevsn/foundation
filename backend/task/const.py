# region status
# Планируется дать студенту
PLANNING = 'planning'
# Студент начал делать
ON_WORK = 'on_work'
# Студент сделал
SOLVED = 'solved'

PLANNING_RU = 'Планируется'
ON_WORK_RU = 'В работе'
SOLVED_RU = 'Решено'

TASK_STATUS_CHOICES = (
    (PLANNING, PLANNING_RU),
    (ON_WORK, ON_WORK_RU),
    (SOLVED, SOLVED_RU),
)

# endregion

# region mark

NOT_DONE = 'not done'
BAD = 'bad'
GOOD = 'good'
GRATE = 'grate'

NOT_DONE_RU = 'Не сделано'
BAD_RU = 'Плохо'
GOOD_RU = 'Хорошо'
GRATE_RU = 'Отлично'

TASK_MARK_CHOICES = (
    (NOT_DONE, NOT_DONE_RU),
    (BAD, BAD_RU),
    (GOOD, GOOD_RU),
    (GRATE, GRATE_RU),
)

# endregion
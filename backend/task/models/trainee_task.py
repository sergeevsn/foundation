from django.db import models
from backend.task import const


# class StudentTraineeTask(models.Model):
#     student = models.OneToOneField(
#         to='TraineeProfile',
#         verbose_name='Студент',
#         related_name='trainee_tasks',
#         blank=False, null=False,
#         on_delete=models.CASCADE(),
#     )
#     task = models.ForeignKey(
#         to='Task',
#         verbose_name="Задача",
#         on_delete=models.SET_NULL(),
#     )
#     start_date = models.DateField(
#         verbose_name='Дата начала',
#         null=True, blank=True
#     )
#     end_date = models.DateField(
#         verbose_name='Дата завершения',
#         null=True, blank=True
#     )
#
#     mark = models.CharField(
#         verbose_name='Итог',
#         max_length=256,
#         choices=const.TASK_STATUS_CHOICES,
#         null=True, blank=True
#     )
#     status = models.CharField(
#         verbose_name='Статус',
#         max_length=256,
#         choices=const.TASK_STATUS_CHOICES,
#         null=True, blank=True
#     )
#
#     solution_comment = models.TextField(
#         verbose_name='Комментарий к решению',
#         max_length=1024,
#         blank=True, null=True
#     )
#
#     class Meta:
#         verbose_name = 'Задания студента'
#         verbose_name_plural = 'Задания студента'
#
#     def __str__(self):
#         return self.student
#
#
# class TraineeTask(models.Model):
#     is_typical = models.BooleanField(
#         verbose_name='Типовая задача',
#         default=False
#     )
#     title = models.CharField(
#         verbose_name='Заголовок',
#         max_length=256
#     )
#     description = models.TextField(
#         verbose_name='Описание',
#         max_length=2048,
#         blank=True, null=True
#     )
#
#     hardness = models.IntegerField(
#         verbose_name='Сложность',
#         null=True, blank=True
#     )
#
#     class Meta:
#         verbose_name = 'Задача'
#         verbose_name_plural = 'Задачи'
#
#     def __str__(self):
#         return self.student
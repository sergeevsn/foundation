from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
class ExtendedUser(AbstractUser):
    """Расширенная модель пользователя"""
    # Есть стандартные поля first_name, last_name, но для единообразия ФИО будем хранить в поле fio
    fio = models.CharField(
        verbose_name='ФИО',
        max_length=256,
        null=False, blank=False
    )

    class Meta(AbstractUser.Meta):
        ordering = ('fio',)

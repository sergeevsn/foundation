from django.http import JsonResponse
from rest_framework.viewsets import ModelViewSet

from .models import Student
from .serializers import StudentSerializer


class StudentsViewSet(ModelViewSet):
    serializer_class = StudentSerializer

    def get_queryset(self):
        return Student.objects.all()



def index(request):
    students = []

    for student in Student.objects.all():
        students.append({
            'fio': student.fio,
            'description': student.description,
            'course': student.course,
        })
    return JsonResponse(students, safe=False)
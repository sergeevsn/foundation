from django.urls import path
from rest_framework import routers

from .views import index, StudentsViewSet

router = routers.DefaultRouter()
router.register('biography', StudentsViewSet, basename='biography')

urlpatterns = router.urls
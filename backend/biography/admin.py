from django.contrib import admin

# Register your models here.
from .models import Student, WorkingProfile

admin.site.register(
    Student,
)

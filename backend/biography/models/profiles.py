from django.db import models


# Разделение рабочей и студенческой истории

class WorkingProfile(models.Model):
    student = models.OneToOneField(
        to='Student',
        verbose_name='Студент',
        related_name='working_profile',
        blank=False, null=False,
        on_delete=models.CASCADE
    )
    start_date = models.DateField(
        verbose_name='Дата поступления',
    )

    class Meta:
        verbose_name = 'Рабочая анкета'
        verbose_name_plural = 'Рабочие анкеты'

    def __str__(self):
        return self.student


class TraineeProfile(models.Model):
    student = models.OneToOneField(
        to='Student',
        verbose_name='Студент',
        related_name='trainee_profile',
        blank=False, null=False,
        on_delete=models.CASCADE
    )
    start_date = models.DateField(
        verbose_name='Дата поступления',
    )

    class Meta:
        verbose_name = 'История стажировки'
        verbose_name_plural = 'Истории стажировок'

    def __str__(self):
        return self.student

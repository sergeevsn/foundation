from django.db import models

# Create your models here.
from .. import const


class Student(models.Model):
    fio = models.CharField(
        verbose_name='ФИО',
        max_length=256
    )
    birthday = models.DateField(
        verbose_name='Дата рождения',
        null=True, blank=True
    )
    description = models.TextField(
        verbose_name='Описание',
        max_length=1024,
        blank=True, null=True
    )
    education_place = models.CharField(
        verbose_name='Место обучения',
        max_length=256
    )
    course = models.IntegerField(
        verbose_name='Курс',
        null=True, blank=True
    )
    extendent_education_place = models.CharField(
        verbose_name='Дополнительное образование',
        max_length=256
    )
    experience = models.TextField(
        verbose_name='Опыт работы',
        max_length=1024,
        blank=True, null=True
    )
    future_plans = models.TextField(
        verbose_name='Планы на будущее',
        max_length=1024,
        null=True, blank=True,
    )
    status = models.CharField(
        verbose_name='Статус',
        max_length=256,
        choices=const.STATUS_CHOICES,
        null=True, blank=True
    )

    class Meta:
        verbose_name = 'Студент'
        verbose_name_plural = 'Студента'
        ordering = ('fio', '-course')

    def __str__(self):
        return self.fio

    def check_status(self, status):
        return self.status == status

    @property
    def is_considering(self):
        return self.check_status(const.CONSIDERATION)

    @property
    def is_intern(self):
        return self.check_status(const.INTERN)

    @property
    def is_trainee(self):
        return self.check_status(const.TRAINEE)

    @property
    def is_on_contract(self):
        return self.check_status(const.CONTRACT)

    @property
    def is_staff(self):
        return self.check_status(const.STAFF)

    @property
    def is_dismissed(self):
        return self.check_status(const.DISMISSED)
